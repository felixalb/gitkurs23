# Gitkurs23

### [Kommandolinje "Cheat sheet"](https://education.github.com/git-cheat-sheet-education.pdf)

### [Presentasjon](https://md.feal.no/p/ax1JdKC3w#/7)

### "Workshop"

1. Klon dette repoet
1. Lag en ny branch
1. Lag filen `guruer/dittnavn`
1. Sjekk status (`git status`)
1. Stage filen
1. Sjekk status (`git status`)
1. Commit endringen
1. Se på endringene (For eksempel `git log` og `git show`)
1. Push til remote
1. Lag en Merge Request

